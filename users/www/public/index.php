<?php
#ini_set('error_reporting', E_ALL);
#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);

use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Router;
use Phalcon\Http\Response;
use Phalcon\Db\Adapter\Pdo\Sqlite;

define('BASE_PATH', dirname(__DIR__));


$app = new Micro();


$app['db'] = new Sqlite(
    [
        "dbname" => BASE_PATH . "/tmp/db.sqlite",
    ]
);

class migration
{
    protected  $db;
    public function migration($db)
    {
        $this->db = $db;
    }

    public function run() {
        if(!$this->db->tableExists('users'))
        {
            $this->db->createTable(
                "users",
                null,
                array(
                    "columns" => array(
                        new Column("login",
                            array(
                                "type"    => Column::TYPE_VARCHAR,
                                "size"    => 10,
                                "notNull" => true,
                            )
                        ),
                        new Column("password",
                            array(
                                "type"    => Column::TYPE_VARCHAR,
                                "size"    => 10,
                                "notNull" => true,
                            )
                        )
                    )
                )
            );

            $sql     = "INSERT INTO `users`(`login`, `password`) VALUES ('?', '?')";
            $success = $this->db->query(
                $sql,
                ["admin", "admin"]
            );
        }
    }
}

$app['migration'] = new migration($app['db']);

$app->post(
    '/api',
    function () use ($app) {

        $app['migration']->run();

        if ($app->request->isPost()) {

            $response = new Response();
            $data = $app->request->getRawBody();

            $data = json_decode($data, true);
            $params = $data['params'];

            list($controller, $method) = explode(".", $data['method']);

            if ($method == 'auth') {

                $query = $app['db']->query(
                    "SELECT * FROM users where login = ? and password = ?",
                    [
                        $params['login'],
                        $params['password']
                    ]
                );

               $user = $query->fetch();

                $response->setContentType('application/json', 'UTF-8');
                if ($user) {
                    $resp = $response->setJsonContent(array(
                        'version' => '2.0',
                        'success' => true,
                        'data' => array('message' => 'успешная авторизация', 'id' => $user["index"])
                    ))->send();

                } else {
                    $resp = $response->setJsonContent(array(
                        'version' => '2.0',
                        'success' => false,
                        'data' => array('message' => 'не верный логин или пароль')
                    ))->send();

                }
                return $resp;
            }


        }


        echo "json-rpc";
    }
);


$app->handle();
