<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class ProductsMigration_100 extends Migration
{
    public function up()
    {

        $this->db->createTable(
            "users",
            null,
            array(
                "columns" => array(
                    new Column("login",
                        array(
                            "type"    => Column::TYPE_VARCHAR,
                            "size"    => 10,
                            "notNull" => true,
                        )
                    ),
                    new Column("password",
                        array(
                            "type"    => Column::TYPE_VARCHAR,
                            "size"    => 10,
                            "notNull" => true,
                        )
                    )
                )
            )
        );

        $sql     = "INSERT INTO `users`(`login`, `password`) VALUES ('?', '?')";
        $success = $this->db->query(
            $sql,
            ["admin", "admin"]
        );
    }
}